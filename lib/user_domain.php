<?php
/**
 * Copyright (c) 2016 Jérôme Bourgeois <jerome@xlinfo.fr>
 * Based on the work of Robin Appelman <icewind@owncloud.com>
 * This file is licensed under the Affero General Public License version 3 or
 * later.
 * See the COPYING-README file from Robin Appelman.
 */

class OC_User_DOMAIN extends \OCA\user_external\Base {
	private $mailbox;
	private $domainName;

	public function __construct($mailbox,$domainName='',$group='',$quota='') {
		parent::__construct($mailbox);
		$this->mailbox=$mailbox;
		$this->domainName=$domainName;
		$this->group=$group;
		$this->quota = $quota;

	}

	/**
	 ** @brief Check if the password is correct
	 ** @param $uid The username
	 ** @param $password The password
	 ** @returns true/false
	 **
	 ** Check if the password is correct without logging in the user
	 **/
	public function checkPassword($uid, $password) {
		if (!function_exists('imap_open')) {
			OCP\Util::writeLog('user_external', 'ERROR: PHP imap extension is not installed', OCP\Util::ERROR);
			return false;
		}
		if($this->domainName != '')
			$uid = $uid . "@". $this->domainName;

		$mbox = @imap_open($this->mailbox, $uid, $password);
		imap_errors();
		imap_alerts();
		if($mbox) {
			imap_close($mbox);
			$uid = explode("@", $uid)[0];
			$this->storeUser($uid);
			$userManager=\OC::$server->getUserManager();
			$user=$userManager->get($uid);
			if($user->getEMailAddress()== "")
				$user->setEMailAddress($uid .'@'. $this->domainName);

			if(! \OC::$server->getGroupManager()->groupExists($this->group))
				\OC::$server->getGroupManager()->createGroup($this->group);
			$group = \OC::$server->getGroupManager()->get($this->group);
			if(!$group->inGroup($user))
				$group->addUser($user);
			$config = \OC::$server->getConfig();
			//jusqu'a nextcloud 12
			//if($config->getUserValue($uid, 'files', 'quota') === null) 
			if(!$config->getUserValue($uid, 'files', 'quota')) 
				$config->setUserValue($uid, 'files', 'quota', $this->quota);
			return $uid;
		}else{
			return false;
		}
	}

}

