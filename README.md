# user_imap

Authentification imap pour nextcloud améliorée
- gère les adresses mail
- attribue un groupe par défaut avec un quota le cas échéant

Attention l’application user_external doit être préalablement activée, sinon, cette application est inopérante.

