/* A rajouter */
 'user_backends' => 
  array (
    0 => 
    array (
      'class' => 'OC_User_DOMAIN',
      'arguments' => 
      array (
        0 => '{serveur_imap:993/imap/ssl/novalidate-cert}',
        1 => 'domain.tld',
        2 => 'nom_du_groupe',
        3 => '512 MB',
      ),
    ),
  ),
'app.mail.accounts.default' => 
  array (
    'email' => '%EMAIL%',
    'imapHost' => 'serveur_imap',
    'imapPort' => 993,
    'imapUser' => '%EMAIL%',
    'imapSslMode' => 'ssl',
    'smtpHost' => 'serveur_smtp',
    'smtpPort' => 465,
    'smtpUser' => '%EMAIL%',
    'smtpSslMode' => 'ssl',
  ),
